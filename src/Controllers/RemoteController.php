<?php

namespace Pobble\Remote\Controllers;

use Illuminate\Http\Request;

class RemoteController {
    
    public function __invoke(Request $request, $method)
    {
        $class = $this->resolve($method);
        return $class->handle($request);
    }

    private function resolve($method)
    {
        return resolve('Framework\Http\Remote\\' . studly_case($method));
    }

}