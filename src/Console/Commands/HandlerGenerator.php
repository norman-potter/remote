<?php

namespace Pobble\Remote\Console\Commands;

use File;
use Illuminate\Console\Command;

class HandlerGenerator extends Command
{
    protected $namespace = 'Remote';

    protected $signature = 'remote:handler {name}';

    protected $description = 'Create a new remote handler class';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $template = $this->generate();
        $this->save($template);

        $this->info("Remote handler Framework\Http\Remote\\" . $this->argument('name') . " created");
    }

    private function generate()
    {
        $template = file_get_contents(__DIR__ . '\..\templates\handler.template');

        $template = str_replace('{{namespace}}', 'Framework\Http\Remote', $template);
        $template = str_replace('{{name}}', $this->argument('name'), $template);

        return $template;
    }

    public function save($template)
    {
        File::put(app_path('Http/Remote/') . $this->argument('name') . '.php', $template);
    }

}
