<?php

namespace Pobble\Remote\Providers;

use Illuminate\Support\ServiceProvider;

class RemoteServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Pobble\Remote\Console\Commands\HandlerGenerator',
    ];

    public function boot()
    {
        $this->publishes([
            $this->path('config') => base_path('config')
        ]);
    }

    public function register()
    {
        $routes = $this->path('routes.php');
        $config = $this->path('config/pobble.remote.php');

        $this->loadRoutesFrom($routes);
        $this->mergeConfigFrom($config, 'pobble.remote');
        $this->commands($this->commands);
    }

    private function path($add = null)
    {
        return (__DIR__ . '/../') . ($add ?: $add);
    }
}
