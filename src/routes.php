<?php

$controller = config('pobble.remote.controller', 'Pobble\Remote\Controllers\RemoteController');

Route::any('/pobble/remote/{method}', $controller);